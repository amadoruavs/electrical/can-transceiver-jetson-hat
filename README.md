# Jetson CAN Transceiver HAT

Behold, a simple and cheap CAN transceiver HAT for the Jetson TX2. This little breakout board should meet all of your CAN transceiving needs:

* Classic CAN and CAN-FD support
* 5V and 3V3 MCU support
* Automotive-grade CAN transceiver
* 4 pin Terminal Block (5.08mm) connector for connecting to most Botz Boards
* Testpoints for debugging
* Silent mode
* Terminator (enabled using solder bridge)
* Small form factor
* Cheap (~$4 USD in parts)
* Easy to make

The transceiver used is the TI TCAN337GDR CAN/CAN-FD transceiver.

Designed using KiCad. Developed by Vincent Wang and the AmadorUAVs team.

# Usage
## Terminal Block
The terminal block side is self explanatory - plug in the CAN cables into the terminal blocks to continue the chain.

## Pins
The 30-pin header on the board is designed to fit onto the 30-pin auxiliary header on the Jetson TX2 carrier board.

## LEDs
The board has 4 LEDs:
- IC POWER indicator, which indicates the board and transceiver are receiving 3V3 power from the Jetson board.
- BUS POWER indicator, which indicates the VBUS pin on one of the terminal blocks is powered.
- CAN1_SILENT indicator, located above the CAN1 transceiver, which lights up when the transceiver is put into SILENT (RX only) mode.
- CAN2_SILENT indicator, located above the CAN2 transceiver, which lights up when the transceiver is put into SILENT (RX only) mode.

## Terminator
The breakout board includes 120 Ohm split-termination CAN terminators for more convenient CAN-bus termination.
To use this, (don't plug in a second cable to the terminal block) and bridge the corresponding solder bridge labeled "CAN0 TERM" or "CAN1 TERM".

## Power via CAN
Unlike the original transceiver board, this board *does not support power over CAN*. This is because the Jetson TX2 draws a large amount
of power and should be powered with a separate power supply, via the 5V pin on the 40-pin header.

# Pictures
![Top](img/top.png)

![Side](img/angled.png)

![Bottom](img/bottom.png)
